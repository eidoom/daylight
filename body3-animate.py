#!/usr/bin/env python3

import functools
import numpy, matplotlib.pyplot, matplotlib.animation


def update(figs, data, frame):
    for (scat, plt), datum in zip(zip(*figs), data):
        plt.set_data(datum[max(0, frame - 50) : frame].T)
        scat.set_offsets(datum[frame])
    return figs


if __name__ == "__main__":
    d = numpy.loadtxt("body3.csv", delimiter=",")[::500]

    fig, ax = matplotlib.pyplot.subplots(
        figsize=(6.4, 6.4),
        tight_layout=True,
    )

    data = [d[:, [1, 2]], d[:, [3, 4]], d[:, [5, 6]]]
    scat = [ax.scatter(*d[0]) for d in data]
    plt = [ax.plot(*d[0], alpha=0.5)[0] for d in data]

    x, y = d[:, [1, 3, 5]], d[:, [2, 4, 6]]
    xl, xh = numpy.min(x), numpy.max(x)
    yl, yh = numpy.min(y), numpy.max(y)
    pad = 0.01
    xp, yp = pad * (xh - xl), pad * (yh - yl)
    ax.set_xlim(xl - xp, xh + xp)
    ax.set_ylim(yl - yp, yh + yp)
    ax.set_aspect("equal")

    anim = matplotlib.animation.FuncAnimation(
        fig=fig,
        func=functools.partial(update, (scat, plt), data),
        frames=len(d),
        interval=1,
    )

    matplotlib.pyplot.show()
