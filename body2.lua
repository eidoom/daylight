#!/usr/bin/env luajit

local gravitational_constant = 6.674e-11 -- m^3 kg^-1 s^-2

local function acceleration_gravity(r, m)
	return r ~= 0 and gravitational_constant * m / r^2 or 0
end

local function centre_of_mass(m1, m2, r1, r2)
	return (m1 * r1 + m2 * r2) / (m1 + m2)
end

local mass_solar = 1.98847e30 -- kg
local mass_earth = 5.9722e24 -- kg
local au = 1.495978707e11 -- m

local a = { 1.3, 0.7 }
local b = 4
local c = 5e3

-- use custom metatable
local bodies = {
	{
		m = a[1] * mass_solar,
		r = { x = -b * au, y = 0 },
		v = { x = 0, y = c/a[1] },
		a = { x = 0, y = 0 },
	},
	{
		m = a[2] * mass_solar,
		r = { x = b * au, y = 0 },
		v = { x = 0, y = -c/a[2] },
		a = { x = 0, y = 0 },
	}
}


local dt = 1e2

-- algorithm: velocity verlet without half-step velocity (a depends on x(t) only)
for t = 0, 5e8, dt do
	-- move ibody by gravitational attraction of jbody
	for i, ibody in ipairs(bodies) do
		for j, jbody in ipairs(bodies) do
			-- optimisation: do doubles together
			if i ~= j then
				for k, q in pairs(ibody.r) do
					-- r(t+dt)
					ibody.r[k] = q + ibody.v[k] * dt + 0.5 * ibody.a[k] * dt^2
				end

				local r = { x = ibody.r.x - jbody.r.x, y = ibody.r.y - jbody.r.y }
				rr = math.sqrt(r.x^2 + r.y^2) -- temp de-local
				local aa = acceleration_gravity(rr, jbody.m) / rr

				for k, q in pairs(r) do
					-- a(t+dt)
					local an = -q * aa
					-- v(t+dt)
					ibody.v[k] = ibody.v[k] + 0.5 * (ibody.a[k] + an) * dt
					ibody.a[k] = an
				end
			end
		end
	end
	local com = {
		x = centre_of_mass(bodies[1].m, bodies[2].m, bodies[1].r.x, bodies[2].r.x),
		y = centre_of_mass(bodies[1].m, bodies[2].m, bodies[1].r.y, bodies[2].r.y)
	}
	print(t..","..bodies[1].r.x..","..bodies[1].r.y..","..bodies[2].r.x..","..bodies[2].r.y..","..com.x..","..com.y..","..rr)
end
