#!/usr/bin/env python3

import math
import numpy, matplotlib.pyplot, matplotlib.cm


if __name__ == "__main__":
    d = numpy.fromfile("body3-eigen.bin").reshape((-1, 7))

    au = 1.495978707e11  # m
    radiant_flux_earth = 1 / 2 / au**2  # L_solar r_earth

    t, x1, y1, x2, y2, x3, y3 = d.T

    xo = x3 - x1
    yo = y3 - y1
    r1 = numpy.hypot(xo, yo)

    x = x3 - x2
    y = y3 - y2
    r2 = numpy.hypot(x, y)

    # exoplanet m = 1.2 m_earth, assume same density => exo r = cbrt(1.2) r_earth
    l1 = 1.2 ** (1 / 3) * 1.1**4 / 2 / r1**2 / radiant_flux_earth  # l_earth
    l2 = 1.2 ** (1 / 3) * 0.9**4 / 2 / r2**2 / radiant_flux_earth

    year_earth = 31558149.7635456
    ty = t / year_earth

    print("Plotting...")
    fig, (ax1, ax2) = matplotlib.pyplot.subplots(
        nrows=2,
        figsize=(6.4, 6.4),
        tight_layout=True,
    )

    colours = matplotlib.cm.tab10(range(3))

    ax1.plot(x3, y3, color=colours[2])
    ax1.plot(x1, y1, color=colours[0])
    ax1.plot(x2, y2, color=colours[1])

    ax1.set_aspect("equal")
    ax1.set_xlabel("x")
    ax1.set_xlabel("y")

    # ax2.plot(ty, l1, label="$\ell_1$")
    # ax2.plot(ty, l2, label="$\ell_2$")
    ax2.plot(ty, l1 + l2, label="$\ell_1+\ell_2$")

    ax2.set_xlabel("Time (year$_\oplus$)")
    # ax2.set_ylabel("Radiant flux from star $i$ ($\ell_{\oplus}$)")
    ax2.set_ylabel("Radiant flux from stars ($\ell_{\oplus}$)")
    # ax2.legend()

    matplotlib.pyplot.show()
