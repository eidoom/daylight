#!/usr/bin/env python3

import math
import numpy, matplotlib.pyplot, matplotlib.cm


if __name__ == "__main__":
    au = 1.495978707e11  # m
    radiant_flux_earth = 1 / 2 / au**2  # L_solar r_earth
    # g = 6.674e-11
    # mass_earth = 5.9722e24
    # m_exo = 1.2 * mass_earth
    # radius_earth = 6.3781e6
    # r_exo = 1.2 ** (1 / 3) * radius_earth
    # grav_exo = g * m_exo / r_exo**2
    # print(grav_exo / (g * mass_earth / radius_earth**2))

    d = numpy.loadtxt("body3.csv", delimiter=",")
    d = d[::int(1e3)]

    # t, x1, y1, x2, y2, x3, y3, ax, ay = d.T
    t, x1, y1, x2, y2, x3, y3 = d.T

    xo = x3 - x1
    yo = y3 - y1
    r1 = numpy.hypot(xo, yo)

    x = x3 - x2
    y = y3 - y2
    r2 = numpy.hypot(x, y)

    # exoplanet m = 1.2 m_earth, assume same density => exo r = cbrt(1.2) r_earth
    l1 = 1.2 ** (1 / 3) * 1.1**4 / 2 / r1**2 / radiant_flux_earth  # l_earth
    l2 = 1.2 ** (1 / 3) * 0.9**4 / 2 / r2**2 / radiant_flux_earth

    l = l2 / l1

    # angle between stars as viewed from planet
    # p = numpy.arccos((x * xo + y * yo) / (r1 * r2))

    # th = math.pi + numpy.arctan2(y, x) # angle of planet wrt the star it orbits (shows years)
    # year_exo = t[numpy.isclose(th, 0, atol=1e-2)][0]
    year_earth = 31558149.7635456
    ty = t / year_earth
    # print(year_exo/year_earth)

    # a = numpy.hypot(ax, ay) / grav_exo

    print("Plotting...")
    # fig, (ax1, ax2, ax3, ax4) = matplotlib.pyplot.subplots(
    fig, (ax1, ax2) = matplotlib.pyplot.subplots(
        nrows=2,
        figsize=(6.4, 6.4),
        tight_layout=True,
    )

    colours = matplotlib.cm.tab10(range(3))

    ax1.plot(x3, y3, color=colours[2])
    ax1.plot(x1, y1, color=colours[0])
    ax1.plot(x2, y2, color=colours[1])

    ax1.set_aspect("equal")
    ax1.set_xlabel("x")
    ax1.set_xlabel("y")

    ax2.plot(ty, l1, label="$\ell_1$")
    ax2.plot(ty, l2, label="$\ell_2$")
    ax2.plot(ty, l1 + l2, label="$\ell_1+\ell_2$")

    ax2.set_xlabel("Time (year$_\oplus$)")
    ax2.set_ylabel("Radiant flux from star $i$ ($\ell_{\oplus}$)")
    ax2.legend()

    # ax3.plot(ty, l)

    # ax3.set_xlabel("Time (year$_\oplus$)")
    # ax3.set_ylabel("$\ell_2/\ell_1$")

    # ax4.plot(ty, a)

    # ax4.plot(ty, p)

    # ax4.set_ylabel(r"$\Delta\phi$")
    # ax4.set_xlabel("Time (year$_\oplus$)")

    matplotlib.pyplot.show()

    # fig.savefig("body3.pdf", bbox_inches="tight")

    # fig, ax = matplotlib.pyplot.subplots(
    #     figsize=(6.4, 6.4),
    #     tight_layout=True,
    # )

    # ax.scatter(0, 0, color=colours[2])
    # ax.plot(x, y, color=colours[0])
    # ax.plot(xo, yo, color=colours[1])

    # fig.savefig("body3-gaiacentric.pdf", bbox_inches="tight")
