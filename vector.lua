#!/usr/bin/env luajit

local Vector = {}

local meta = {}

meta.__index = meta

function meta:__call(x_, y_)
	return setmetatable({x = x_ or 0, y = y_ or 0}, meta)
end

function meta:__add(v)
	return Vector(self.x + v.x, self.y + v.y)
end

function meta:__sub(v)
	return Vector(self.x - v.x, self.y - v.y)
end

function meta:__unm()
	return Vector(-self.x, -self.y)
end

function meta:__mul(v)
	return self.x * v.x + self.y * v.y
end

function meta:__tostring()
	return self.x .. "," .. self.y
end

function meta:__eq(v)
	return self.x == v.x and self.y == v.y
end

function meta:__lt(v)
	return self:magnitude() < v:magnitude()
end

function meta:__le(v)
	return self:magnitude() <= v:magnitude()
end

function meta:norm()
	return math.sqrt(self * self)
end

function meta:scale(s)
	return Vector(s * self.x, s * self.y)
end

setmetatable(Vector, meta)

return Vector
