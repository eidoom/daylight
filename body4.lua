#!/usr/bin/env luajit

local gravitational_constant = 6.674e-11 -- m^3 kg^-1 s^-2
local mass_solar = 1.98847e30 -- kg
local mass_earth = 5.9722e24 -- kg
local mass_lunar = 7.342e22 -- kg
local au = 1.495978707e11 -- m
local sma_lunar = 3.84399e8 -- m

local function instantaneous_orbital_speed(m1,m2,r)
	return math.sqrt(gravitational_constant*(m1+m2)/r)
end

local a = { 1.1, 0.9 }
local b = 9 * au
local c = 5e3
local d1 = math.sqrt(a[1]) * au
local d2 = math.sqrt(a[2]) * au

local body = {
	{
		m = a[1] * mass_solar,
		r = { x = 0, y = 0 },
		v = { x = 0, y = c/a[1] },
		a = { x = 0, y = 0 },
	},
	{
		m = a[2] * mass_solar,
		r = { x, y = 0 },
		v = { x = 0, y = -c/a[2] },
		a = { x = 0, y = 0 },
	},
	{
		m = 1.2 * mass_earth,
		r = { x, y = 0 },
		v = { x = 0, y },
		a = { x = 0, y = 0 },
	},
	{
		m = mass_lunar,
		r = { x, y = 0 },
		v = { x = 0, y },
		a = { x = 0, y = 0 },
	},
	{
		m = 1.1 * mass_earth,
		r = { x, y = 0 },
		v = { x = 0, y },
		a = { x = 0, y = 0 },
	},
}

body[2].r.x = body[1].r.x + b

body[3].r.x = body[2].r.x + d2
body[3].v.y = body[2].v.y - instantaneous_orbital_speed(body[3].m, body[2].m, d2)

local md = 1.5 * sma_lunar
body[4].r.x = body[3].r.x + md
body[4].v.y = body[3].v.y - instantaneous_orbital_speed(body[4].m, body[3].m, md)

body[5].r.x = body[1].r.x + d1
body[5].v.y = body[1].v.y - instantaneous_orbital_speed(body[5].m, body[1].m, d1)

local file = io.open("body4.csv", "w+")

local dt = 1e3

-- algorithm: velocity verlet without half-step velocity (a depends on x(t) only)
for t = 0, 4e8, dt do
	for i, ibody in ipairs(body) do
		for k, q in pairs(ibody.r) do
			ibody.r[k] = q + ibody.v[k] * dt + 0.5 * ibody.a[k] * dt^2
		end
	end

	for i, ibody in ipairs(body) do
		local ai = { x = 0, y = 0 }

		for j, jbody in ipairs(body) do
			if j ~= i then
				local r = { x = ibody.r.x - jbody.r.x, y = ibody.r.y - jbody.r.y }
				local aij = gravitational_constant * jbody.m / math.sqrt(r.x^2 + r.y^2)^3

				for k, q in pairs(r) do
					ai[k] = ai[k] - q * aij
				end
			end
		end

		for k, an in pairs(ai) do
			ibody.v[k] = ibody.v[k] + 0.5 * (ibody.a[k] + an) * dt
			ibody.a[k] = an
		end
	end

	file:write(t..","..body[1].r.x..","..body[1].r.y..","..body[2].r.x..","..body[2].r.y..","..body[3].r.x..","..body[3].r.y..","..body[4].r.x..","..body[4].r.y..","..body[5].r.x..","..body[5].r.y.."\n")
end

file:close()
