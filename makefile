CXX?=g++
CXXFLAGS=-std=c++20 -O3 -march=native -mtune=native
CPPFLAGS=`pkg-config eigen3 --cflags`
LDFLAGS=

SRCS = $(wildcard *.cpp)
PRGS = $(basename $(SRCS))

.PHONY: all clean

all: $(PRGS)

body3-eigen: body3-eigen.o
	$(CXX) -o $@ $^ $(LDFLAGS)

body3-eigen.o: body3-eigen.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -o $@ -c $<

clean:
	-rm *.o $(PRGS)


