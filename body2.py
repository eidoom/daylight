#!/usr/bin/env python3

import sys
import numpy, matplotlib.pyplot, matplotlib.patches

if __name__ == "__main__":
    d = numpy.loadtxt(sys.stdin, delimiter=",")

    t, x0, y0, x1, y1, cx, cy, r = d.T

    fig, ax = matplotlib.pyplot.subplots()

    ax.set_aspect("equal")

    ax.plot(x0, y0)
    ax.plot(x1, y1)
    ax.plot(numpy.mean(cx), numpy.mean(cy), marker=".")

    fig.savefig("body2-orbit.pdf")

    fig, ax = matplotlib.pyplot.subplots()

    ax.plot(t, r)

    fig.savefig("body2-separation.pdf")
