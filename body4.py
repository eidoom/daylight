#!/usr/bin/env python3

import numpy, matplotlib.pyplot

if __name__ == "__main__":
    d = numpy.loadtxt("body4.csv", delimiter=",")

    t, x0, y0, x1, y1, x2, y2, x3, y3, x4, y4 = d.T

    fig, ax = matplotlib.pyplot.subplots()

    ax.set_aspect("equal")

    ax.plot(x0, y0)
    ax.plot(x1, y1)
    ax.plot(x2, y2)
    ax.plot(x3, y3)
    ax.plot(x4, y4)

    fig.savefig("body4.pdf")
