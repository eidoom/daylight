#!/usr/bin/env python3

import math, functools
import numpy, matplotlib.pyplot, matplotlib.animation


def update(figs, data, scat2, data2, frame):
    for (scat, plt), datum in zip(zip(*figs), data):
        plt.set_data(datum[max(0, frame - 50) : frame].T)
        scat.set_offsets(datum[frame])
    for scat, datum in zip(scat2, data2):
        scat.set_offsets((datum[frame], 0))
    return figs


if __name__ == "__main__":
    start = 1000
    number = 500
    step = 10
    d = numpy.loadtxt("body3.csv", delimiter=",")[start : start + step * number : step]

    t, x1, y1, x2, y2, x3, y3 = d.T

    xo = x3 - x1
    yo = y3 - y1
    r1 = numpy.hypot(xo, yo)

    x = x3 - x2
    y = y3 - y2
    r2 = numpy.hypot(x, y)

    # vector v points out from planet surface like an observer looking up at the sky
    day_earth = 8.6e4
    w = 2 * math.pi / day_earth
    vx = numpy.cos(w * t)
    vy = numpy.sin(w * t)

    t1 = numpy.arccos((vx * xo + vy * yo) / r1)
    t2 = numpy.arccos((vx * x + vy * y) / r2)

    fig, (ax1, ax2) = matplotlib.pyplot.subplots(
        nrows=2,
        height_ratios=(1, 0.1),
        figsize=(6.4, 6.4),
        tight_layout=True,
    )

    data = [d[:, [1, 2]], d[:, [3, 4]], d[:, [5, 6]]]
    scat = [ax1.scatter(*d[0]) for d in data]
    plt = [ax1.plot(*d[0], alpha=0.5)[0] for d in data]

    x, y = d[:, [1, 3, 5]], d[:, [2, 4, 6]]
    xl, xh = numpy.min(x), numpy.max(x)
    yl, yh = numpy.min(y), numpy.max(y)
    pad = 0.01
    xp, yp = pad * (xh - xl), pad * (yh - yl)
    ax1.set_xlim(xl - xp, xh + xp)
    ax1.set_ylim(yl - yp, yh + yp)
    ax1.set_aspect("equal")

    data2 = [t1, t2]
    scat2 = [ax2.scatter(d[0], 0) for d in data2]

    ax2.set_xlim(0, math.pi)
    ax2.tick_params(axis="y", which="both", left=False, labelleft=False)

    anim = matplotlib.animation.FuncAnimation(
        fig=fig,
        func=functools.partial(update, (scat, plt), data, scat2, data2),
        frames=len(d),
        interval=1,
    )

    print("Generating animation")
    anim.save("body3.html", "html")
