#!/usr/bin/env julia

using LinearAlgebra

using Plots

function main()
    g = 6.674e-11
    ms = 1.98847e30
    me = 5.9722e24
    au = 1.495978707e11

    a1 = 1.1
    a2 = 0.9
    b1 = 9
    c1 = 1.2
    d1 = 5e3

    m = [a1*ms a2*ms c1*me]

    q = zeros(Float64, 3, 3, 2)

    q[2, 1, 1] = q[1, 1, 1] + b1 * au;
    q[3, 1, 1] = q[2, 1, 1] + sqrt(a2) * au;

    q[1, 2, 2] = d1 / a1
    q[2, 2, 2] = -d1 / a2
    q[3, 2, 2] = q[2, 2, 2] - sqrt(g * (m[2] + m[3]) / (sqrt(a2) * au));

    dt = 1e3
    out = []

    for t = 0:dt:2e9
        for b in eachslice(q, dims=1)
            b[1,:] += b[2,:] * dt + 0.5 * b[3,:] * dt^2
            b[2,:] += 0.5 * b[3,:] * dt
            b[3,:] = [0 0]
        end

        for (i, bi) in enumerate(eachslice(q, dims=1))
            for (j, bj) in enumerate(eachslice(q, dims=1))
                if j > i
                    rij = bi[1,:] - bj[1,:]
                    r = norm(rij)
                    rij *= g / r^3
                    bi[3,:] -= m[j] * rij
                    bj[3,:] += m[i] * rij
                end
            end
        end

        for b in eachslice(q, dims=1)
            b[2,:] += 0.5 * b[3,:] * dt
        end

        push!(out, q[:, 1, :])
    end

    plot(
         [
          ([o[1,1] for o in out], [o[1,2] for o in out]),
          ([o[2,1] for o in out], [o[2,2] for o in out]),
          ([o[3,1] for o in out], [o[3,2] for o in out])
         ],
         labels=["s1" "s2" "p"]
        )

    savefig("body3-julia.pdf")
end

main()
