#include "main.hpp"

#include "camera.hpp"

Camera::Camera(float d, std::array<Eigen::Vector3d, N>& r_)
    : pixelSize{d, d}, centrePoint{0.f, 0.f}, follow{}, following{false},
      sensitivity{10.f}, r{r_} {}

// Eigen::Array2f Camera::win2phys(Eigen::Array2f winCoords) {
//   return pixelSize * (winCoords - winSize() * 0.5f);
// }

Eigen::Array2f Camera::phys2win(Eigen::Array2f physCoords) {
  return ((physCoords - centrePoint) / pixelSize + winSize() * 0.5f);
}

void Camera::zoom(float scroll) { pixelSize *= 1.f - 0.1f * scroll; }

void Camera::centre(int x, int y) {
  centrePoint -=
      pixelSize * Eigen::Array2f{static_cast<float>(x), static_cast<float>(y)};
}

void Camera::snap(int x, int y) {
  following = false;
  const Eigen::Vector2f p{static_cast<float>(x), static_cast<float>(y)};

  for (int i{0}; i < N; ++i) {
    const Eigen::Vector2f q{phys2win(r[i].head(2).cast<float>())};

    const float s{(p - q).norm()};

    if (s < sensitivity) {
      follow = i;
      following = true;
      return;
    }
  }
}

void Camera::tick() {
  if (following) {
    centrePoint = r[follow].head(2).cast<float>();
  }
}
