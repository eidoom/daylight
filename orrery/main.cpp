#include <array>
#include <cmath>
#include <iostream>

#include <Eigen/Core>
#include <SDL.h>

#include "camera.hpp"
#include "main.hpp"

// const double GRAVITATIONAL_CONSTANT{6.674e-11}, AU{1.495978707e11};

const double TT{1e-4}; // render time step
const int UU{1000};    // simulation ticks per frame

const int initWinSide{720};

std::array<double, N> m;
std::array<Eigen::Vector3d, N> r, v, a;

SDL_Window* window;
SDL_Renderer* renderer;
Uint32 lastUpdate = SDL_GetTicks();

Eigen::Array2f winSize() {
  int w, h;
  SDL_GetRendererOutputSize(renderer, &w, &h);
  return {static_cast<float>(w), static_cast<float>(h)};
}

Camera camera(0.1, r);

void init_phys() {
  // data from https://astronomy.stackexchange.com/a/13491/10792

  // index:
  // sun
  // mercury
  // venus
  // earth (earth-moon barycentre)
  // mars
  // jupiter
  // saturn
  // uranus
  // neptune
  // pluto

  // using units of length=au, time=day is better for rounding errors than SI
  // by reduce scale variation in intermediate values
  // so what about mass? solar mass units?

  // GM, au^3/day^2
  m = {
      0.295912208285591100e-03, 0.491248045036476000e-10,
      0.724345233264412000e-09, 0.888769244512563400e-09,
      0.954954869555077000e-10, 0.282534584083387000e-06,
      0.845970607324503000e-07, 0.129202482578296000e-07,
      0.152435734788511000e-07, 0.217844105197418000e-11,
  };

  // au
  r = std::array<Eigen::Vector3d, N>{{
      {0.450250878464055400e-02, 0.767076427091007100e-03,
       0.266057917766977700e-03},
      {0.361762716560282000e+00, -0.907819721567660000e-01,
       -0.857149725627511600e-01},
      {0.612751940835072200e+00, -0.348365369033622200e+00,
       -0.195278286675943800e+00},
      {0.120517414101384700e+00, -0.925838474769148700e+00,
       -0.401540226453152200e+00},
      {-0.110186077148798200e+00, -0.132759945030298300e+01,
       -0.605889140484291400e+00},
      {-0.537970676855393700e+01, -0.830481326563397800e+00,
       -0.224828874426565500e+00},
      {0.789439068290953100e+01, 0.459647805517127300e+01,
       0.155869584283190000e+01},
      {-0.182654022538723600e+02, -0.116195541867587000e+01,
       -0.250106057721338000e+00},
      {-0.160550357802333700e+02, -0.239421915598547100e+02,
       -0.940015796880239500e+01},
      {-0.304833137671838400e+02, -0.872405556841050100e+00,
       0.891157617249955100e+01},
  }};

  // au/day
  v = std::array<Eigen::Vector3d, N>{{
      {-0.351749536075523100e-06, 0.517762640983340500e-05,
       0.222910217891202900e-05},
      {0.336749397200575900e-02, 0.248945205576834300e-01,
       0.129463004097040900e-01},
      {0.109520684235282300e-01, 0.156176842678676800e-01,
       0.633110570297786400e-02},
      {0.168112683097837900e-01, 0.174830923073434400e-02,
       0.758202897383129100e-03},
      {0.144816530570475700e-01, 0.242463076836468600e-03,
       -0.281520727924338800e-03},
      {0.109201259423733800e-02, -0.651811661280738400e-02,
       -0.282078276229867900e-02},
      {-0.321755651650091600e-02, 0.433581034174662600e-02,
       0.192864631686015500e-02},
      {0.221190391015614700e-03, -0.376247500810884400e-02,
       -0.165101502742995000e-02},
      {0.264276984798005500e-02, -0.149831255054097800e-02,
       -0.679041960802913300e-03},
      {0.322207373497780800e-03, -0.314357639364532900e-02,
       -0.107794975959731300e-02},
  }};

  // au/day^2
  for (int i{0}; i < N; ++i) {
    a[i] = {0., 0., 0.};
  }
}

bool init_sdl() {
  if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
    std::cerr << "Error initializing SDL: " << SDL_GetError() << std::endl;
    return false;
  }

  window = SDL_CreateWindow("Orrery", SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED, initWinSide, initWinSide,
                            SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
  if (!window) {
    std::cerr << "Error creating window: " << SDL_GetError() << std::endl;
    return false;
  }

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  if (!renderer) {
    std::cerr << "Error creating renderer: " << SDL_GetError() << std::endl;
    return false;
  }

  return true;
}

void loop_phys(double dt) {
  // set the time step (proportional to render dt)
  dt *= TT;

  // do UU simulation ticks for every rendered frame
  // since simulation more stable for smaller dt
  for (int u{0}; u < UU; ++u) {
    // velocity verlet integration (2nd order symplectic integrator)
    for (int i{0}; i < N; ++i) {
      const Eigen::Vector3d vn{0.5 * a[i] * dt};
      r[i] += v[i] * dt + vn * dt;
      v[i] += vn;
      a[i] = {0., 0., 0.};
    }

    // do symmetric acceleration calculation
    for (int i{0}; i < N; ++i) {
      for (int j{i + 1}; j < N; ++j) {
        Eigen::Vector3d rij{r[i] - r[j]};
        const double r{rij.norm()};
        // nb G is hard coded in the mass
        rij /= r * r * r;
        a[i] -= m[j] * rij;
        a[j] += m[i] * rij;
      }
    }

    for (int i{0}; i < N; ++i) {
      v[i] += 0.5 * a[i] * dt;
    }
  }
}

bool loop() {
  SDL_Event e;

  while (SDL_PollEvent(&e) != 0) {
    switch (e.type) {
      case SDL_QUIT:
        return false;
      case SDL_KEYDOWN:
        switch (e.key.keysym.sym) {
          case SDLK_q:
            return false;
        }
        break;
      case SDL_MOUSEBUTTONDOWN:
        switch (e.button.button) {
          case SDL_BUTTON_LEFT:
            camera.snap(e.button.x, e.button.y);
            break;
        }
        break;
      case SDL_MOUSEWHEEL:
        camera.zoom(e.wheel.preciseY);
        break;
      case SDL_MOUSEMOTION:
        if (e.motion.state == SDL_PRESSED) {
          camera.centre(e.motion.xrel, e.motion.yrel);
        }
        break;
    }
  }

  camera.tick();

  Uint32 current = SDL_GetTicks();

  double dT = current - lastUpdate; // ms

  loop_phys(dT);

  lastUpdate = current;

  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  SDL_RenderClear(renderer);

  std::array<SDL_FRect, N> ps;
  for (int i{0}; i < N; ++i) {
    const Eigen::Array2f xy{camera.phys2win(r[i].head(2).cast<float>())};
    const float l{3.};
    ps[i] = {xy[0], xy[1], l, l};
  }
  SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
  SDL_RenderFillRectsF(renderer, ps.data(), N);

  SDL_RenderPresent(renderer);

  return true;
}

void quit() {
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);

  renderer = NULL;
  window = NULL;

  SDL_Quit();
}

int main() {
  init_phys();

  if (!init_sdl()) {
    return EXIT_FAILURE;
  }

  while (loop()) {
    SDL_Delay(20);
  }

  quit();
}
