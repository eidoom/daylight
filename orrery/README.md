# orrery

```shell
sudo dnf install SDL2-devel
make
./main
```

## TODO

* use SDL3
* make code cleaner, probably classes are easiest
* more bodies
* quadtree
* add view panning, tilt and zooming
* add real-time changing of number of simulation ticks per frame
* use higher-order integrator
* draw objects as something other than squares
