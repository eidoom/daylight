#pragma once

#include <Eigen/Core>

#include "main.hpp"

class Camera {
public:
  Camera(float d, std::array<Eigen::Vector3d, N>& r);

  // Eigen::Array2f win2phys(Eigen::Array2f winCoords);

  Eigen::Array2f phys2win(Eigen::Array2f physCoords);

  void zoom(float scroll);

  void centre(int x, int y);

  void snap(int x, int y);

  void tick();

private:
  Eigen::Array2f pixelSize;   // au per pixel
  Eigen::Array2f centrePoint; // centre point

  int follow; // index of planet being followed by camera
  bool following;
  float sensitivity; // snap sensitivity
                     // (radius of circle where click activates on a planet)

  std::array<Eigen::Vector3d, N>& r;
};
