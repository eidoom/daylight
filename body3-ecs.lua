#!/usr/bin/env luajit

local tiny = require("tiny")
local Vector = require("vector") -- this has terrible performance

local gravitational_constant = 6.674e-11 -- mass^3 kg^-1 s^-2
local mass_solar = 1.98847e30 -- kg
local mass_earth = 5.9722e24 -- kg
local au = 1.495978707e11 -- mass

local function instantaneous_orbital_speed(m1,m2,position)
	return math.sqrt(gravitational_constant*(m1+m2)/position)
end

local a = { 1.1, 0.9 }
local b = 9 * au
local c = 5e3
local d = math.sqrt(a[2]) * au

local A = {
		mass = a[1] * mass_solar,
		position = Vector(),
		velocity = Vector( 0, c / a[1] ),
		acceleration = Vector(),
}

local B = {
		mass = a[2] * mass_solar,
		position = Vector( A.position.x + b, 0 ),
		velocity = Vector( 0, -c / a[2] ),
		acceleration = Vector(),
}

local Bb = {
		mass = 1.2 * mass_earth,
		position = Vector( B.position.x + d, 0 ),
		velocity = Vector(),
		acceleration = Vector(),
}

Bb.velocity.y = B.velocity.y - instantaneous_orbital_speed(Bb.mass, B.mass, d)

local system = tiny.processingSystem()

system.filter = tiny.requireAll("mass", "position", "velocity", "acceleration")

function system:process(entity, dt)
		entity.position = entity.position + entity.velocity:scale(dt) + entity.acceleration:scale(0.5 * dt^2)
		entity.velocity = entity.velocity + entity.acceleration:scale(0.5 * dt)
		entity.acceleration = Vector()

		i = system.indices[entity]

		-- maybe can dedouble using system no processingSystem?
		for j, other in pairs(self.entities) do
			if j ~= i then
				local position = entity.position - other.position
				local aij = gravitational_constant * other.mass / position:norm()^3
				entity.acceleration = entity.acceleration -	position:scale(aij)
			end
		end

		entity.velocity = entity.velocity + entity.acceleration:scale(0.5 * dt)
end

local world = tiny.world(system, A, B, Bb)

local file = io.open("body3-ecs.csv", "w+")

local dt = 1e3

for t = 0, 2e9, dt do
	world:update(dt)
	file:write(t..","..tostring(A.position)..","..tostring(B.position)..","..tostring(Bb.position).."\n")
end

file:close()
