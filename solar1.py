#!/usr/bin/env python3

import sys
import numpy, matplotlib.pyplot, matplotlib.patches

if __name__ == "__main__":
    d = numpy.loadtxt(sys.stdin, delimiter=",")

    x, y = d.T
    r = numpy.sqrt(x**2 + y**2)

    print(numpy.min(r), numpy.max(r))

    sun = matplotlib.patches.Circle((0, 0), 6.957e8, color="orange")
    earth = matplotlib.patches.Circle((x[-1], y[-1]), 6.3781e6, color="green")

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 6.4))

    ax.plot(x, y)

    ax.add_patch(sun)
    ax.add_patch(earth)

    fig.savefig("solar1.pdf")
