#include <Eigen/Core>
#include <array>
#include <cmath>
#include <fstream>

int main()
{
	const double gravitational_constant { 6.674e-11 },
			mass_solar { 1.98847e30 },
			mass_earth { 5.9722e24 },
			au { 1.495978707e11 };

	constexpr int n { 3 };

	const double a1 { 1.1 },
			a2 { 0.9 },
			b1 { 9. },
			c1 { 1.2 },
			d1 { 5e3 };

	const std::array<double, n> m {
		a1 * mass_solar,
		a2 * mass_solar,
		c1 * mass_earth,
	};

	std::array<Eigen::Vector2d, n> r { {
			{ 0., 0. },
			{ 0., 0. },
			{ 0., 0. },
	} };

	r[1][0] = r[0][0] + b1 * au;
	r[2][0] = r[1][0] + std::sqrt(a2) * au;

	std::array<Eigen::Vector2d, n> v { {
			{ 0., d1 / a1 },
			{ 0., -d1 / a2 },
			{ 0., 0. },
	} };

	v[2][1] = v[1][1] - std::sqrt(gravitational_constant * (m[1] + m[2]) / (std::sqrt(a2) * au));

	std::array<Eigen::Vector2d, n> a { {
			{ 0., 0. },
			{ 0., 0. },
			{ 0., 0. },
	} };

	std::ofstream output { "body3-eigen.bin", std::ios::binary };

	const double dt { 5 };

	for (double t { 0. }; t < 1.5e10; t += dt) {
		for (int i { 0 }; i < n; ++i) {
			r[i] += v[i] * dt + 0.5 * a[i] * dt * dt;
			v[i] += 0.5 * a[i] * dt;
			a[i] = { 0., 0. };
		}

		for (int i { 0 }; i < n; ++i) {
			for (int j { i + 1 }; j < n; ++j) {
				Eigen::Vector2d rij { r[i] - r[j] };
				double r { rij.norm() };
				rij *= gravitational_constant / (r * r * r);
				a[i] -= m[j] * rij;
				a[j] += m[i] * rij;
			}
		}

		for (int i { 0 }; i < n; ++i) {
			v[i] += 0.5 * a[i] * dt;
		}

		if (std::fmod(t, 1e3) == 0.) {
			output.write(reinterpret_cast<const char*>(&t), sizeof t);
			for (const Eigen::Vector2d ri : r) {
				for (const double x : ri) {
					output.write(reinterpret_cast<const char*>(&x), sizeof x);
				}
			}
		}
	}
}
