#!/usr/bin/env luajit

local function area_circle(radius)  
	return 2 * math.pi * radius
end

local function surface_area_sphere(radius)  
	return 4 * math.pi * radius^2
end

local gravitational_constant = 6.674e-11 -- m^3 kg^-1 s^-2 https://en.wikipedia.org/wiki/Gravitational_constant
local stefan_boltzmann_constant = 5.670374419e-8 -- W m^-2 K^-4 https://en.wikipedia.org/wiki/Stefan%E2%80%93Boltzmann_law

-- https://en.wikipedia.org/wiki/Orbital_speed#Instantaneous_orbital_speed https://en.wikipedia.org/wiki/Vis-viva_equation
local function instantaneous_orbital_speed(m1,m2,a,r)
	return math.sqrt(gravitational_constant*(m1+m2)*(2/r-1/a))
end

-- luminosity is radiant flux (full spectrum)
local luminosity_solar = 3.828e26 -- W https://en.wikipedia.org/wiki/Solar_luminosity
local mass_solar = 1.98847e30 -- kg https://en.wikipedia.org/wiki/Solar_mass
local radius_solar = 6.957e8 -- m (equatorial) https://en.wikipedia.org/wiki/Solar_radius

local mass_earth = 5.9722e24 -- kg https://en.wikipedia.org/wiki/Earth_mass
local mean_orbital_radius_earth = 1.495978707e11 -- m https://en.wikipedia.org/wiki/Astronomical_unit
local radius_earth = 6.3781e6 -- m https://en.wikipedia.org/wiki/Earth_radius

local day_earth = 8.64e4 -- s https://en.wikipedia.org/wiki/Day
local year_earth = 3.65256363004e2 * day_earth -- s (sidereal) https://en.wikipedia.org/wiki/Year
local perihelion_earth = 1.4709807e11 -- m https://en.wikipedia.org/wiki/Apsis#Earth_perihelion_and_aphelion
-- alternative 1.47098291e11 https://en.wikipedia.org/wiki/Apsis#Other_planets
local aphelion_earth = 1.520977e11 -- m
-- 1.52098233e11 (but mean of these alternaive apsides further from 1 AU: 391300 vs 14300)
-- https://en.wikipedia.org/wiki/Earth%27s_orbit#Events_in_the_orbit
local semi_major_axis_earth = 1.4960e11 -- m
local eccentricity_earth = 1.67086e-2
-- local semi_minor_axis_earth = semi_major_axis_earth * math.sqrt(1 - eccentricity_earth^2)
local perihelion_speed_earth = instantaneous_orbital_speed(mass_solar, mass_earth, semi_major_axis_earth, perihelion_earth) -- m s^-1
local aphelion_speed_earth = instantaneous_orbital_speed(mass_solar, mass_earth, semi_major_axis_earth, aphelion_earth) -- m s^-1

local irradiance_solar_earth = luminosity_solar / surface_area_sphere(mean_orbital_radius_earth) -- W m^-2 https://en.wikipedia.org/wiki/Solar_irradiance https://en.wikipedia.org/wiki/Irradiance
local radiant_flux_solar_earth = irradiance_solar_earth * area_circle(radius_earth) -- W https://en.wikipedia.org/wiki/Radiant_flux

-- print(math.sqrt(4*math.pi^2*semi_major_axis_earth^3/gravitational_constant/(mass_solar+mass_earth))-year_earth)
-- print((luminosity_solar/(surface_area_sphere(radius_solar)*stefan_boltzmann_constant))^(1/4))

-- local function sign(n)
--     return n > 0 and 1 or n < 0 and -1 or 0
-- end

-- https://en.wikipedia.org/wiki/Newton%27s_law_of_universal_gravitation
local function acceleration_gravity(r, m)
	return r ~= 0 and gravitational_constant * m / r^2 or 0
end

local earth = {
	x = { x = perihelion_earth, y = 0 },
	v = { x = 0, y = perihelion_speed_earth },
	a = { x = 0, y = 0 },
}

-- https://en.wikipedia.org/wiki/Verlet_integration#Velocity_Verlet (a(x): a from interaction potential only, not velocity)
-- https://en.wikipedia.org/wiki/Leapfrog_integration#Algorithm (synchronised: dt constant)
-- https://en.wikipedia.org/wiki/Symplectic_integrator#A_second-order_example
local dt = 1e2

print(earth.x.x..","..earth.x.y)
-- algorithm: velocity verlet without half-step velocity (a depends on x(t) only)
for t = 0, 3 * year_earth, dt do
	for k, q in pairs(earth.x) do
		-- x(t+dt)
		earth.x[k] = q + earth.v[k] * dt + 0.5 * earth.a[k] * dt^2
	end

	local r = math.sqrt(earth.x.x^2 + earth.x.y^2)
	local aa = acceleration_gravity(r, mass_solar) / r

	for k, q in pairs(earth.x) do
		-- a(t+dt)
		local an = -q * aa
		-- v(t+dt)
		earth.v[k] = earth.v[k] + 0.5 * (earth.a[k] + an) * dt
		earth.a[k] = an
	end

	print(earth.x.x..","..earth.x.y)
end
