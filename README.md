# [daylight](https://gitlab.com/eidoom/daylight)

## Usage

```shell
./solar1.lua > solar1.csv
cat solar1.csv | ./solar1.py
```

```shell
./body2.lua | ./body2.py
```

```shell
./body3.lua && ./body3.py
```

```shell
sudo dnf install eigen3-devel
make && ./body3-eigen && ./body3-eigen.py
```

## Ideas

* rotate point on planet surface by [centripetal force](https://en.wikipedia.org/wiki/Centripetal_force)
* use [line/circle](https://jeffreythompson.org/collision-detection/line-circle.php) collision detection to check if line-of-sight ray from planet surface can see star (day) or is occluded by planet (night)

## Observations

Simulation `body3`.
Stars `s2` (lower mass, wider orbit) and `s1` (higher mass, smaller orbit) form a binary system `b`.
The non-circumbinary planet `p` orbits `s2` and rotates on its axis prograde.
The `p` year (orbit around `s1`) wobbles in length a little.
The `b` cycle takes approximately 11 `p` years.
The `b` cycle has its most interesting part when `s2` is near and within the ellipse inscribed by the path of `s1`, which is around a two year period, where `p` has a higher speed.
There are periods of `b` cycles when `p`'s path is symmetric about the x-axis (two loops inside `s1` ellipse), and periods when it is not (one loop overlapping `s1` ellipse edge.
The acceleration experienced on `p` due to its motion in `b` peaks at 0.2% `g` (gravity on `p` [= 1.06 earth gravity]).

Symmetric periods.
During the slow part of the `b` cycle, `p` gets closer and further from `s2` over the course of its year.
The oscillation has a greater amplitude every second `b` cycle.
Radiant flux oscillates between as much as ~0.6 and ~1.1 $l_\oplus$ in "high" cycles and as little as ~0.79 to ~0.82 in "low" cycles.
In the fast subcycle, this is exaggerated, typically with the middle spiking to ~1.4-2.2 $l_\oplus$ in the summer.
Winter is warmer too due to the higher flux from the closer `s1`.
`s2` is always brighter in the sky, peaking ~50 times brighter in slow summer and with a trough of ~2 times in fast winter. 
`s1` chases `s2` across the sky of `p`, with the angle between them going to zero (`s2` eclipses `s1`) in slow winter (both stars to same side of `p`) and antipodal in slow summer since the stars are to opposite sides of `p` (inverted in the fast subcycle).

Non-symmetric periods.
Yearly flux fluctuations constantly much higher.

System becomes unstable by ~450 (earth) years.
Physical or numerical?
