#!/usr/bin/env luajit

local gravitational_constant = 6.674e-11 -- m^3 kg^-1 s^-2
local mass_solar = 1.98847e30 -- kg
local mass_earth = 5.9722e24 -- kg
local au = 1.495978707e11 -- m

local function instantaneous_orbital_speed(m1,m2,r)
	return math.sqrt(gravitational_constant*(m1+m2)/r)
end

local a = { 1.1, 0.9 }
local b = 9 * au
local c = 5e3
local i = 2
local d = math.sqrt(a[i]) * au

local body = {
	{
		m = a[1] * mass_solar,
		r = { x = 0, y = 0 },
		v = { x = 0, y = c/a[1] },
		a = { x = 0, y = 0 },
	},
	{
		m = a[2] * mass_solar,
		r = { x, y = 0 },
		v = { x = 0, y = -c/a[2] },
		a = { x = 0, y = 0 },
	},
	{
		m = 1.2 * mass_earth,
		r = { x, y = 0 },
		v = { x = 0, y },
		a = { x = 0, y = 0 },
	}
}

body[2].r.x = body[1].r.x + b

body[3].r.x = body[i].r.x + d
body[3].v.y = body[i].v.y - instantaneous_orbital_speed(body[3].m, body[i].m, d)

local file = io.open("body3.csv", "w+")

local dt = 1e3

-- algorithm: velocity verlet with half-step velocity
for t = 0, 2e9, dt do
	for i, ibody in ipairs(body) do
		for k, q in pairs(ibody.r) do
			-- r(t+dt)
			ibody.r[k] = q + ibody.v[k] * dt + 0.5 * ibody.a[k] * dt^2
			-- v(t+dt/2)
			ibody.v[k] = ibody.v[k] + 0.5 * ibody.a[k] * dt
			ibody.a[k] = 0
		end
	end

	for i, ibody in ipairs(body) do
		for j, jbody in ipairs(body) do
			if j > i then
				local r = { x = ibody.r.x - jbody.r.x, y = ibody.r.y - jbody.r.y }
				local aij = gravitational_constant / math.sqrt(r.x^2 + r.y^2)^3

				for k, q in pairs(r) do
					-- a(t+dt)
					ibody.a[k] = ibody.a[k] - q * aij * jbody.m
					jbody.a[k] = jbody.a[k] + q * aij * ibody.m
				end
			end
		end
	end

	for i, ibody in ipairs(body) do
		for k, ak in pairs(ibody.a) do
			-- v(t+dt)
			ibody.v[k] = ibody.v[k] + 0.5 * ak * dt
		end
	end

	file:write(t..","..body[1].r.x..","..body[1].r.y..","..body[2].r.x..","..body[2].r.y..","..body[3].r.x..","..body[3].r.y.."\n")
end

file:close()
